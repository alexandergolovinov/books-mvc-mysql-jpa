package com.example.springmvcjpa.controllers;

import com.example.springmvcjpa.entity.Book;
import com.example.springmvcjpa.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class MainController {

    public static final String REDIRECT_MAIN = "redirect:/";
    @Autowired
    private BookService bookService;

    @GetMapping("/")
    public String mainPage(Model model) {
        List<Book> books = bookService.findAllBooks();
        model.addAttribute("books", books);
        return "index";
    }

    @GetMapping(value = "/edit/{id}")
    public String editBookPage(@PathVariable(value = "id", required = false) long id, Model model) {
        Book book = bookService.findBookById(id);
        Date date = book.getPurchaseDate();
        model.addAttribute("purchaseDate", date);
        model.addAttribute("book", book);
        return "/editbook";
    }

    @PostMapping(value = "/update/{id}")
    public String updateBook(@PathVariable("id") long id,
                                  @ModelAttribute("book") Book book) {
        Book updateBook = bookService.findBookById(id);
        bookService.update(book, updateBook);
        return REDIRECT_MAIN;
    }

    @GetMapping(value = "/newbook")
    public String createNewBook(@ModelAttribute("book") Book book) {
        return "newbook";
    }

    @PostMapping(value = "/newbook")
    public String createBook(@Valid Book book, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return "newbook";
        }
        bookService.save(book);
        return REDIRECT_MAIN;
    }

    @GetMapping(value = "/delete/{id}")
    public String deleteBook(@PathVariable("id") long id){
        Book book = bookService.findBookById(id);
        bookService.delete(book);
        return REDIRECT_MAIN;
    }

    @InitBinder
    public void initBindeer(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class,
                new CustomDateEditor(new SimpleDateFormat("yyyy-mm-dd"), false));
    }

}
