package com.example.springmvcjpa.service;

import com.example.springmvcjpa.entity.Book;
import com.example.springmvcjpa.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class BookService {

    private BookRepository bookRepository;

    @Autowired
    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public List<Book> findAllBooks() {
        List<Book> books = new ArrayList<>();
        Iterable<Book> bookList = bookRepository.findAll();
        bookList.forEach(books::add);
        return books;
    }

    public Book findBookById(Long id) {
        Optional<Book> book = bookRepository.findById(id);
        if (book.isPresent()) {
            return book.get();
        }
        return null;
    }

    public void update(Book source, Book target) {
        if(!ObjectUtils.isEmpty(source)) {
            target.setId(source.getId());
            target.setName(source.getName());
            target.setAuthor(source.getAuthor());
            target.setPurchaseDate(source.getPurchaseDate());
        }
        bookRepository.save(target);
    }

    public void save(Book book) {
        bookRepository.save(book);
    }


    public void delete(Book book) {
        bookRepository.delete(book);
    }
}
