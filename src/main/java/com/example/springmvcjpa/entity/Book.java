package com.example.springmvcjpa.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "BOOK")
public class Book implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "BOOK_NAME")
    @NotNull
    private String name;

    @Column(name = "BOOK_AUTHOR")
    @NotNull
    private String author;

    @Column(name = "PURCHASE_DATE")
    @Temporal(TemporalType.DATE) //To format Date properly in the DB.
    private Date purchaseDate;

    public Book() {}

    public Book(String name, String author, Date purchaseDate) {
        this.name = name;
        this.author = author;
        this.purchaseDate = purchaseDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", purchaseDate=" + purchaseDate +
                '}';
    }
}
